# README #

### What is this repository for? ###

* WebsiteChecker is a small tool for checking the content of a website periodically
* It's based on a JavaFX-GUI and uses system notifications to display when the pattern in the html source of a website was found (or not found).
* Version 1.0

### How do I get set up? ###
* Download Intellij-Idea
* Clone the repo

* To generate the jfx-jar simply use mvn package and the complete jar will be under <RepoDir>/target/jfx/app

### Dependencies ###

This project uses
* junit
* apache httpclient
* log4j2
* Apache commons-io