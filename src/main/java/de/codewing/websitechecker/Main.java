package de.codewing.websitechecker;

import de.codewing.websitechecker.controller.MainWindowController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Main extends Application {

    Stage primaryStage;

    public static void main(String[] args) {
        launch(Main.class, args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("layouts/layout_root.fxml"));
        primaryStage.setTitle("WebsiteChecker");
        primaryStage.setScene(new Scene(root, 600, 280));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new javafx.scene.image.Image("images/icon_white.png"));


        if (SystemTray.isSupported()) {
            Platform.setImplicitExit(false);
            setupTray();
        }

        if (getParameters().getRaw().contains("hidden")) {
            primaryStage.hide();
        } else {
            primaryStage.show();
        }
    }

    private void setupTray() {

        PopupMenu popup = new PopupMenu();
        MenuItem showItem = new MenuItem("Open");
        MenuItem checkItem = new MenuItem("Check");
        MenuItem exitItem = new MenuItem("Close");

        ActionListener closeListener = e -> System.exit(0);
        ActionListener showListener = e -> Platform.runLater(() -> primaryStage.show());
        ActionListener checkListener = e -> MainWindowController.instance.checkAll();
        showItem.addActionListener(showListener);
        checkItem.addActionListener(checkListener);
        exitItem.addActionListener(closeListener);

        popup.add(showItem);
        popup.add(checkItem);
        popup.add(exitItem);

        SystemTray sTray = SystemTray.getSystemTray();
        Image imageIcon = null;
        try {
            imageIcon = ImageIO.read(getClass().getClassLoader().getResource("images/icon_white.png"));
        } catch (IOException e) {
            System.err.println("Error while reading white icon: " + e);
        }
        TrayIcon icon = new TrayIcon(imageIcon, "WebsiteChecker", popup);

        try {
            sTray.add(icon);
        } catch (AWTException e) {
            System.err.println(e);
        }

        primaryStage.setOnCloseRequest(we -> primaryStage.hide());
    }

}
