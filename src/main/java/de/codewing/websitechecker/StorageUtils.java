package de.codewing.websitechecker;

import de.codewing.websitechecker.model.TaskData;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Created by codewing on 22.01.2016.
 */
public class StorageUtils {

    private static File saveFile = new File("tasks.txt");

    public static void saveTasks(List<TaskData> tasks) {
        StringBuilder sb = new StringBuilder();
        tasks.forEach(task -> sb.append(task.toCSV() + "\n"));

        writeStringToFile(sb.toString());
    }

    public static ObservableList<TaskData> loadTasks() {
        ObservableList<TaskData> tasks = FXCollections.observableArrayList();

        if (saveFile.exists()) {
            try (BufferedReader br = new BufferedReader(new FileReader(saveFile))) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] split = line.split(",");
                    if (split.length == 6) {
                        tasks.add(new TaskData(split[0], split[1], "true".equals(split[2]), split[3], split[4], split[5]));
                    }
                }
            } catch (IOException e) {
                System.err.println("Error while reading the file: " + e);
            }
        }

        return tasks;
    }

    private static void writeStringToFile(String text) {
        try (FileOutputStream fos = new FileOutputStream(saveFile);
             OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
            osw.write(text);
        } catch (IOException e) {
            System.err.println("Error while writing the file: " + e);
        }
    }
}
