package de.codewing.websitechecker.controller;

import de.codewing.websitechecker.model.TaskData;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.util.Set;

/**
 * Created by codewing on 21.01.2016.
 */
public class EditDialog {
    public static Dialog<TaskData> createDialog(TaskData currentElement, Set<String> currentNames) {
        Dialog<TaskData> dialog = new Dialog<>();
        dialog.setGraphic(null);

        ButtonType saveButtonType = new ButtonType("Save", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(saveButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField name = new TextField();
        name.setPromptText("Name");
        dialog.getDialogPane().lookupButton(saveButtonType).setDisable(currentElement == null);
        name.textProperty().addListener((observable, oldValue, newValue) -> {
            if (currentNames == null || currentElement != null && currentElement.getName().equals(newValue) && !newValue.equals("") || !currentNames.contains(newValue) && !newValue.equals("")) {
                dialog.getDialogPane().lookupButton(saveButtonType).setDisable(false);
            } else {
                dialog.getDialogPane().lookupButton(saveButtonType).setDisable(true);
            }
        });

        TextField website = new TextField();
        website.setPromptText("Website");

        TextField params = new TextField();
        params.setPromptText("Parameter");

        ComboBox cbSubmitType = new ComboBox<>(FXCollections.observableArrayList("Post", "Get"));
        cbSubmitType.getSelectionModel().select(0);

        TextField pattern = new TextField();
        pattern.setPromptText("Pattern");

        TextField message = new TextField();
        message.setPromptText("Message");

        if (currentElement != null) {
            dialog.setTitle("Edit " + currentElement.getName());

            name.setText(currentElement.getName());
            website.setText(currentElement.getWebsite());
            params.setText(currentElement.getParams());
            cbSubmitType.getSelectionModel().select(currentElement.isPost() ? 0 : 1);
            pattern.setText(currentElement.getWebsiteContains());
            message.setText(currentElement.getMessage());
        } else {
            dialog.setTitle("Add new scheduled task");
        }

        grid.add(new Label("Name:"), 0, 0);
        grid.add(name, 1, 0);

        grid.add(new Label("Website:"), 0, 1);
        grid.add(website, 1, 1);

        grid.add(new Label("HTTP method:"), 0, 2);
        grid.add(cbSubmitType, 1, 2);

        grid.add(new Label("Parameter:"), 0, 3);
        grid.add(params, 1, 3);

        grid.add(new Label("Pattern:"), 0, 4);
        grid.add(pattern, 1, 4);

        grid.add(new Label("Message:"), 0, 5);
        grid.add(message, 1, 5);

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(() -> name.requestFocus());

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == saveButtonType) {
                return new TaskData(name.getText(), website.getText(), cbSubmitType.getSelectionModel().isSelected(0), params.getText(), pattern.getText(), message.getText());
            }
            return null;
        });

        return dialog;
    }
}
