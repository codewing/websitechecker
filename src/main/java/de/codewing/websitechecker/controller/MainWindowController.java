package de.codewing.websitechecker.controller;

import de.codewing.websitechecker.StorageUtils;
import de.codewing.websitechecker.model.CustomListCell;
import de.codewing.websitechecker.model.TaskData;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Optional;
import java.util.Timer;

public class MainWindowController {

    private final static long HOUR_IN_MS = 1000 * 60 * 60;
    public static MainWindowController instance;
    private ObservableList<TaskData> taskElements;
    private HashMap<String, Timer> timerTasks = new HashMap<>();
    @FXML
    private ListView<TaskData> taskList;

    public MainWindowController() {
        if (instance == null) {
            instance = this;
        }
    }

    @FXML
    @SuppressWarnings("unused")
    public void initialize() {
        setupListView();

        addAllTimerTasks();
    }

    private void setupListView() {
        taskElements = StorageUtils.loadTasks();
        taskList.setItems(taskElements);
        taskList.setCellFactory(param -> new CustomListCell());
    }

    @FXML
    public void handleAdd() {
        Dialog<TaskData> editDialog = EditDialog.createDialog(null, timerTasks.keySet());
        Optional<TaskData> result = editDialog.showAndWait();
        result.ifPresent(updatedObject -> {
            taskElements.add(result.get());
            StorageUtils.saveTasks(taskList.getItems());
            addTimerTask(result.get());
        });
    }

    @FXML
    public void handleEdit() {
        TaskData selectedElement = taskList.getSelectionModel().getSelectedItem();
        if (selectedElement == null) {
            return;
        }

        Dialog<TaskData> editDialog = EditDialog.createDialog(selectedElement, timerTasks.keySet());
        Optional<TaskData> result = editDialog.showAndWait();
        result.ifPresent(updatedObject -> {
            int index = taskElements.indexOf(selectedElement);
            taskElements.remove(index);
            taskElements.add(index, result.get());
            StorageUtils.saveTasks(taskList.getItems());
        });
    }

    @FXML
    public void handleDelete() {
        TaskData selectedElement = taskList.getSelectionModel().getSelectedItem();
        if (selectedElement == null) {
            return;
        }

        //remove the task and cancel further executions
        taskElements.remove(selectedElement);
        timerTasks.remove(selectedElement.getName()).cancel();

        StorageUtils.saveTasks(taskList.getItems());
    }

    public void checkAll() {
        taskElements.forEach(te -> {
            WebsiteCheckerTask wc = new WebsiteCheckerTask(te);
            new Thread(wc).start();
        });
    }

    private void addAllTimerTasks() {
        taskElements.forEach(te -> addTimerTask(te));
    }

    private void addTimerTask(TaskData cd) {
        WebsiteCheckerTask wc = new WebsiteCheckerTask(cd);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(wc, Calendar.getInstance().getTime(), HOUR_IN_MS);
        timerTasks.put(cd.getName(), timer);
    }
}
