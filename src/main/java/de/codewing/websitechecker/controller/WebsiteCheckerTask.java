package de.codewing.websitechecker.controller;

import de.codewing.websitechecker.model.TaskData;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;


/**
 * Created by codewing on 21.01.2016.
 */
public class WebsiteCheckerTask extends TimerTask {

    private static final Logger log = LogManager.getLogger(WebsiteCheckerTask.class);


    private TaskData taskData;

    public WebsiteCheckerTask(TaskData taskData) {
        this.taskData = taskData;
    }

    @Override
    public void run() {

        CloseableHttpClient httpclient = HttpClients.createDefault();

        CloseableHttpResponse response = null;
        try {
            if (taskData.isPost()) {
                HttpPost httpPost = new HttpPost(taskData.getWebsite());
                UrlEncodedFormEntity params = new UrlEncodedFormEntity(buildParameters());
                httpPost.setEntity(params);
                response = httpclient.execute(httpPost);
            } else {
                String query = URLEncodedUtils.format(buildParameters(), "utf-8");
                String requestedUrl = String.format("%s?%s", taskData.getWebsite(), query);
                log.info(this.getClass().getName(), "Requested URL: " + requestedUrl);
                HttpGet httpGet = new HttpGet(requestedUrl);
                response = httpclient.execute(httpGet);
            }

            HttpEntity entity = response.getEntity();

            String websiteContent = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8);
            log.info(websiteContent.trim());

            analyzeContent(websiteContent);

            EntityUtils.consume(entity);
            response.close();
        } catch (IOException e) {
            log.error("Error while executing request " + e);
        }
    }

    private void analyzeContent(String content) {
        String[] checks = taskData.getWebsiteContains().split(";");
        for (String check : checks) {
            String stringToCheckFor = check.substring(check.indexOf("'") + 1, check.lastIndexOf("'"));
            if (check.startsWith("!")) {
                if (!content.contains(stringToCheckFor)) {
                    log.info("Content doesn't contain: '" + stringToCheckFor + "' => showMsg");
                    displayMessage();
                }
            } else {
                if (content.contains(stringToCheckFor)) {
                    log.info("Content contains: '" + stringToCheckFor + "' => showMsg");
                    displayMessage();
                }
            }
        }
    }

    private void displayMessage() {
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();
            tray.getTrayIcons()[0].displayMessage(taskData.getName(), taskData.getMessage(), TrayIcon.MessageType.INFO);
        } else {
            log.error(taskData.getName(), "Couldn't get the system tray.");
        }
    }

    private List<NameValuePair> buildParameters() {
        List<NameValuePair> params = new ArrayList<>();

        String[] paramCombo = taskData.getParams().split(";");
        for (String combo : paramCombo) {
            String[] nvp = combo.split("=");
            if (nvp.length == 2) {
                params.add(new BasicNameValuePair(nvp[0], nvp[1]));
            }
        }

        return params;
    }
}
