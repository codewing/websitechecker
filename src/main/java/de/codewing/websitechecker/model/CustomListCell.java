package de.codewing.websitechecker.model;

import javafx.geometry.Orientation;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;

/**
 * Created by codewing on 21.01.2016.
 */
public class CustomListCell extends ListCell<TaskData> {

    HBox container = new HBox();
    Label name = new Label("(unnamed)");
    Label website = new Label("(example.de)");
    CheckBox cbPost = new CheckBox();
    Label websiteParams = new Label("(param1,param2)");
    Label pattern = new Label("(pattern)");
    Separator s1 = new Separator();
    Separator s2 = new Separator();
    Separator s3 = new Separator();
    Separator s4 = new Separator();

    public CustomListCell() {
        super();
        s1.setOrientation(Orientation.VERTICAL);
        s2.setOrientation(Orientation.VERTICAL);
        s3.setOrientation(Orientation.VERTICAL);
        s4.setOrientation(Orientation.VERTICAL);
        cbPost.setDisable(true);
        container.getChildren().addAll(name, s1, website, s2, cbPost, s3, websiteParams, s4, pattern);
    }

    @Override
    protected void updateItem(TaskData item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        if (empty) {
            setGraphic(null);
        } else {
            name.setText(item.getName());
            website.setText(item.getWebsite());
            websiteParams.setText(item.getParams());
            cbPost.setSelected(item.isPost());
            pattern.setText(item.getWebsiteContains());
            setGraphic(container);
        }
    }
}
