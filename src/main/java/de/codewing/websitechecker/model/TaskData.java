package de.codewing.websitechecker.model;

/**
 * Created by codewing on 21.01.2016.
 */
public class TaskData {

    private String name;
    private String website;
    private boolean isPost;
    private String params;
    private String websiteContains;
    private String message;

    public TaskData(String name, String website, boolean isPost, String params, String websiteContains, String message) {
        this.name = name;
        this.website = website;
        this.isPost = isPost;
        this.params = params;
        this.websiteContains = websiteContains;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean isPost() {
        return isPost;
    }

    public void setIsPost(boolean isPost) {
        this.isPost = isPost;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getWebsiteContains() {
        return websiteContains;
    }

    public void setWebsiteContains(String websiteContains) {
        this.websiteContains = websiteContains;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String toCSV() {
        return String.format("%s,%s,%b,%s,%s,%s", name, website, isPost, params, websiteContains, message);
    }
}
