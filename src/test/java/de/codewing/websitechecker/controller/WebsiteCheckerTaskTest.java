package de.codewing.websitechecker.controller;

import de.codewing.websitechecker.model.TaskData;
import junit.framework.TestCase;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Assert;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by codewing on 30.01.2016.
 */
public class WebsiteCheckerTaskTest extends TestCase {

    TaskData tdSimple, tdComplex;
    WebsiteCheckerTask wctSimple, wctComplex;

    public WebsiteCheckerTaskTest() {
        tdSimple = new TaskData("Perso-Test-Nichtvorhanden",
                "https://www17.muenchen.de/Passverfolgung/",
                true,
                "ifNummer=0123456",
                "!'doesn't contain'",
                "Doesn't contain");
        wctSimple = new WebsiteCheckerTask(tdSimple);

        tdComplex = new TaskData("Perso-Test-Nichtvorhanden",
                "https://www17.muenchen.de/Passverfolgung/",
                true,
                "ifNummer=0123456;pbAbfragen=Abfragen",
                "!'doesn't contain'",
                "Doesn't contain");
        wctComplex = new WebsiteCheckerTask(tdComplex);
    }

    public void testBuildParametersSimple() throws Exception {
        Method method = WebsiteCheckerTask.class.getDeclaredMethod("buildParameters");
        method.setAccessible(true);
        List<NameValuePair> valuePairs = (List<NameValuePair>) method.invoke(wctSimple);
        NameValuePair ifNummer = new BasicNameValuePair("ifNummer", "0123456");
        Assert.assertTrue("Expected " + ifNummer, valuePairs.contains(ifNummer));
    }

    public void testBuildParametersComplex() throws Exception {
        Method method = WebsiteCheckerTask.class.getDeclaredMethod("buildParameters");
        method.setAccessible(true);
        List<NameValuePair> valuePairs = (List<NameValuePair>) method.invoke(wctComplex);
        NameValuePair ifNummer = new BasicNameValuePair("ifNummer", "0123456");
        NameValuePair pbAbfragen = new BasicNameValuePair("pbAbfragen", "Abfragen");
        Assert.assertTrue("Expected " + ifNummer, valuePairs.contains(ifNummer));
        Assert.assertTrue("Expected " + pbAbfragen, valuePairs.contains(pbAbfragen));
    }
}
